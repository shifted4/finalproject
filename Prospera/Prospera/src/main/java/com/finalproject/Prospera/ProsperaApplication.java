package com.finalproject.Prospera;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProsperaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProsperaApplication.class, args);
	}

}
