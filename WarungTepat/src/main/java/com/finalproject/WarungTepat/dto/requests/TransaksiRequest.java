package com.finalproject.WarungTepat.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransaksiRequest {
    private  int idTransaksi;
    private int idProduk;
    private long idSupplier;
    private long nikKtp;
    private int harga;
    private int kuantitas;
    private double total;
    private Date tanggal;
    private Date tanggalTerima;
    private boolean status;
    private String statusPesanan;
}
