package com.finalproject.WarungTepat.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Nasabah {
    private Integer id;
    private String nama;
    private Long nikKtp;
    private String email;
    private String password;
    private String noHP;
    private Boolean flagWarungTepat;
}
