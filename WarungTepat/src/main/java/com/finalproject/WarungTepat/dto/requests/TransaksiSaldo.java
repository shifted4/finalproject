package com.finalproject.WarungTepat.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransaksiSaldo {
    private long nikKtp;
    private long nominal;
}
