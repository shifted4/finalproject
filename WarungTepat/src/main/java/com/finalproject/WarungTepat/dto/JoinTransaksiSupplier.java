package com.finalproject.WarungTepat.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JoinTransaksiSupplier {
    private  int idTransaksi;
    private String namaSupplier;
    private String namaProduk;
    private Long nikKtp;
    private int kuantitas;
    private double total;
    private String statusPesanan;
}

