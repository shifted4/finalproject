package com.finalproject.WarungTepat.services;

import com.finalproject.WarungTepat.controllers.NasabahController;
import com.finalproject.WarungTepat.dto.JoinTransaksi;
import com.finalproject.WarungTepat.dto.JoinTransaksiSupplier;
import com.finalproject.WarungTepat.dto.requests.TransaksiRequest;
import com.finalproject.WarungTepat.dto.requests.TransaksiSaldo;

import com.finalproject.WarungTepat.models.entities.Produk;
import com.finalproject.WarungTepat.models.entities.Supplier;
import com.finalproject.WarungTepat.models.entities.Transaksi;
import com.finalproject.WarungTepat.models.repositories.TransaksiRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TransaksiService {
    @Autowired
    private TransaksiRepositoy transaksiRepositoy;

    @Autowired
    private ProdukService produkService;

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private NasabahController nasabahController;

    public Transaksi save(TransaksiRequest transaksiRequest) {
        Transaksi newTransaksi = new Transaksi();
        newTransaksi.setTanggalTransaksi(null);
        newTransaksi.setStatus(false);
        newTransaksi.setStatusPesanan(null);
        newTransaksi.setHarga(transaksiRequest.getHarga());
        newTransaksi.setTotal(transaksiRequest.getHarga() * transaksiRequest.getKuantitas());
        newTransaksi.setKuantitas(transaksiRequest.getKuantitas());
        newTransaksi.setIdProduk(transaksiRequest.getIdProduk());
        newTransaksi.setIdSupplier(transaksiRequest.getIdSupplier());
        newTransaksi.setNikKtp(transaksiRequest.getNikKtp());
        return transaksiRepositoy.save(newTransaksi);
    }

    public Transaksi update(TransaksiRequest transaksiRequest) {
        Transaksi newTransaksi = findOne(transaksiRequest.getIdTransaksi());
        newTransaksi.setTanggalTransaksi(new Date());
        newTransaksi.setStatus(true);
        newTransaksi.setStatusPesanan("pesan");
        return transaksiRepositoy.save(newTransaksi);
    }

    public Transaksi konfirmasi(TransaksiRequest transaksiRequest) {
        Transaksi transaksi = findOne(transaksiRequest.getIdTransaksi());
        transaksi.setStatusPesanan("dikirim");
        return transaksiRepositoy.save(transaksi);
    }

    public Transaksi diterima(TransaksiRequest transaksiRequest) {
        Transaksi newTransaksi = findOne(transaksiRequest.getIdTransaksi());
        newTransaksi.setTanggalTerima(new Date());;
        newTransaksi.setStatusPesanan("diterima");


        Produk produk = produkService.findOne(newTransaksi.getIdProduk());
        produk.setStok(produk.getStok() - newTransaksi.getKuantitas());

        Supplier supplier = supplierService.findOne(newTransaksi.getIdSupplier());
        supplier.setSaldo(supplier.getSaldo() + newTransaksi.getTotal());

        TransaksiSaldo transaksiSaldo = new TransaksiSaldo();
        transaksiSaldo.setNikKtp(newTransaksi.getNikKtp());
        transaksiSaldo.setNominal(newTransaksi.getTotal());

        NasabahController nasabahController1 = new NasabahController();
        nasabahController1.redirectSaldo(transaksiSaldo);

        return transaksiRepositoy.save(newTransaksi);
    }

    public Transaksi findOne(Integer id) {
        Optional<Transaksi> pesanan = transaksiRepositoy.findById(id);
        if (pesanan.isEmpty()) {
            pesanan = Optional.empty();
        }
        return pesanan.get();
    }

    public Iterable<Transaksi> findAll() {
        return transaksiRepositoy.findAll();
    }

    public Iterable<Transaksi> cariStatusByNikKtp( Long nikKtp, String statusPesanan ) {
        return transaksiRepositoy.findAllByNikKtpAndStatusPesanan(nikKtp, statusPesanan);
    }


    public Iterable<Transaksi> cariTransaksiByNikKtp(long nikKtp){
        return transaksiRepositoy.findAllByNikKtp(nikKtp);
    }

    public List<JoinTransaksi> joinTransaksi(Long nik){
        return transaksiRepositoy.getAllTransaksi(nik);
    }

    public List<JoinTransaksiSupplier> joinTransaksiSupplier(String nohp){
        return transaksiRepositoy.getAllTransaksiSupplierByNomorHandphone(nohp);
    }

}
