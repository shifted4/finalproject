package com.finalproject.WarungTepat.services;

import com.finalproject.WarungTepat.models.entities.Supplier;
import com.finalproject.WarungTepat.models.repositories.SupplierRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class SupplierService {

	public static final Logger logger = LoggerFactory.getLogger(SupplierService.class);

	@Autowired
	private SupplierRepository supplierRepository;

	public Supplier save(Supplier supplier){
		return supplierRepository.save(supplier);
	}
	public Supplier findOne(Long id){
		Optional<Supplier> supplier = supplierRepository.findById(id);
		return supplier.orElse(null);
	}

	public Iterable<Supplier> findAll(){
		return supplierRepository.findAll();
	}

	public void removeOne(Long id){
		supplierRepository.deleteById(id);
	}

	public Supplier findByNomorHanphone(String nomorHandphone){
		return supplierRepository.findByNomorHandphone(nomorHandphone);
	}
}