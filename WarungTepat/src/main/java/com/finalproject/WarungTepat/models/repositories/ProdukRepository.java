package com.finalproject.WarungTepat.models.repositories;

import com.finalproject.WarungTepat.models.entities.Produk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ProdukRepository extends JpaRepository<Produk, Integer> {
    List<Produk> findByNamaProduk(String namaProduk);

    List<Produk> findByStatus(boolean status);
    @Query("SELECT p FROM Produk p WHERE p.deleteAt=null")
    List<Produk> findByDeleteAt();

}
