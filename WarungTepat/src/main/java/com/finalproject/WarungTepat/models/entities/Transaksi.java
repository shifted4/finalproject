package com.finalproject.WarungTepat.models.entities;

import com.finalproject.WarungTepat.dto.JoinTransaksi;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;


/*-----------------> JOIN Table Transaksi, Produk, dan Supplier By NikKtp <--------------------------*/
@SqlResultSetMapping(
        name = "transaksiMapping",
        classes = {
                @ConstructorResult(
                        targetClass = JoinTransaksi.class,
                        columns = {
                                @ColumnResult(name = "id_transaksi", type = Integer.class),
                                @ColumnResult(name = "nama_supplier"),
                                @ColumnResult(name = "nama_produk"),
                                @ColumnResult(name = "kuantitas", type = Integer.class),
                                @ColumnResult(name = "total", type = Double.class),
                                @ColumnResult(name = "status_pesanan"),
                        }
                )
        }

)
/*---------------------> Use Native Query Find By NikKtp <-----------------------------*/
@NamedNativeQuery(
        name = "Transaksi.getAllTransaksi",
        query = "SELECT t.id_transaksi, s.nama_supplier, p.nama_produk, t.kuantitas, t.total, 	t.status_pesanan " +
                "FROM transaksi t " +
                "INNER JOIN produk p ON t.id_produk = p.id_produk " +
                "INNER JOIN supplier s  ON t.id_supplier = s.id_supplier " +
                "WHERE t.nik_ktp = :nik" ,
        resultSetMapping = "transaksiMapping"
)


/*-----------------> JOIN Table Transaksi, Produk, dan Supplier By IdSupplier <--------------------------*/
@SqlResultSetMapping(
        name = "transaksiMappingSupplier",
        classes = {
                @ConstructorResult(
                        targetClass = JoinTransaksi.class,
                        columns = {
                                @ColumnResult(name = "id_transaksi", type = Integer.class),
                                @ColumnResult(name = "nama_supplier"),
                                @ColumnResult(name = "nama_produk"),
                                @ColumnResult(name = "nik_ktp", type = Long.class),
                                @ColumnResult(name = "kuantitas", type = Integer.class),
                                @ColumnResult(name = "total", type = Double.class),
                                @ColumnResult(name = "status_pesanan"),
                        }
                )
        }

)
/*---------------------> Use Native Query Find By Nomor Handphone <-----------------------------*/
@NamedNativeQuery(
        name = "Transaksi.getAllTransaksiSupplierByNomorHandphone",
        query = "SELECT t.id_transaksi, s.nama_supplier, p.nama_produk, t.nik_ktp, t.kuantitas, t.total, 	t.status_pesanan " +
                "FROM transaksi t " +
                "INNER JOIN produk p ON t.id_produk = p.id_produk " +
                "INNER JOIN supplier s  ON t.id_supplier = s.id_supplier " +
                "WHERE s.nomor_handphone = :nohp" ,
        resultSetMapping = "transaksiMappingSupplier"
)

@Entity
@Data
@Table(name = "transaksi")
@AllArgsConstructor
@NoArgsConstructor
public class Transaksi {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTransaksi;

    @Column
    private int idProduk;

    @Column
    private long idSupplier;

    @Column
    private long nikKtp;

    @Column
    private int harga;

    @Column
    private int kuantitas;

    @Column
    private int total;

    @Column
    private boolean status;

    @Column
    @Temporal(TemporalType.DATE)
    private Date tanggalTransaksi;

    @Column
    @Temporal(TemporalType.DATE)
    private Date tanggalTerima;

    @Column
    private String statusPesanan;

}
