package com.finalproject.WarungTepat.models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "supplier")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Supplier {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_supplier")
	private long idSupplier;

	@Column
	private String namaSupplier;

	@Column
	private String kotaSupplier;

	@Column(unique = true)
	private String nomorHandphone;

	@Column
	private String password;

	@Column(columnDefinition = "integer default 0")
	private double saldo;

	@Column
	private String alamatSupplier;

	@Column(unique = true)
	private String email;

	@Column(updatable = false)
	@Temporal(TemporalType.DATE)
	@LastModifiedDate
	private Date createdAt;

	@Column
	@Temporal(TemporalType.DATE)
	@LastModifiedDate
	private Date updatedAt;

	@Column()
	@Temporal(TemporalType.DATE)
	private Date deleteAt;

	@PrePersist
	protected void prePersist() {
		if (this.createdAt == null) createdAt = new Date();
		if (this.updatedAt == null) updatedAt = new Date();
	}

}