package com.finalproject.WarungTepat.models.repositories;

import com.finalproject.WarungTepat.dto.JoinTransaksi;
import com.finalproject.WarungTepat.dto.JoinTransaksiSupplier;
import com.finalproject.WarungTepat.models.entities.Transaksi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransaksiRepositoy extends JpaRepository<Transaksi, Integer> {

    Iterable<Transaksi> findAllByNikKtpAndStatusPesanan(Long nikKtp, String statusPesanan);

    Iterable<Transaksi> findAllByNikKtp(long nikKtp);

    @Query(nativeQuery = true)
    List<JoinTransaksi> getAllTransaksi(@Param("nik") Long nik);

    @Query(nativeQuery = true)
    List<JoinTransaksiSupplier> getAllTransaksiSupplierByNomorHandphone(@Param("nohp") String nohp);



}
