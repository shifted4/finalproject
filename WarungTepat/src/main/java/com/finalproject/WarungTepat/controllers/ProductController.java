package com.finalproject.WarungTepat.controllers;

import com.finalproject.WarungTepat.models.entities.Produk;
import com.finalproject.WarungTepat.services.ProdukService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/produk")
@CrossOrigin("*")
public class ProductController {
    @Autowired
    private ProdukService produkService;

    //All Product with delete or not
    @GetMapping("/all-data")
    public Iterable<Produk> semuaProduk(){
        return produkService.findAll();
    }

    //ALL Product
    @GetMapping
    public Iterable<Produk> semuaProdukTanpaDeleteAt(){
        return produkService.findAllWithoutDeleteAt();
    }

    //Find Product By Id
    @GetMapping("/{id}")
    public Produk produk(@PathVariable("id") Integer id){
        if(id == null){
            throw new NullPointerException("Id tidak ditemukan ! ");
        }
        Produk produk = produkService.findOne(id);
        if(produk.getDeleteAt() != null){
            throw  new RuntimeException("Produk Sudah dihapus !");
        }
        return produk;
    }


}
