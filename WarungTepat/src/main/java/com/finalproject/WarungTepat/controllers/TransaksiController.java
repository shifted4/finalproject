package com.finalproject.WarungTepat.controllers;

import com.finalproject.WarungTepat.dto.JoinTransaksi;
import com.finalproject.WarungTepat.dto.JoinTransaksiSupplier;
import com.finalproject.WarungTepat.dto.requests.TransaksiRequest;
import com.finalproject.WarungTepat.dto.responses.ResponseData;
import com.finalproject.WarungTepat.models.entities.Transaksi;
import com.finalproject.WarungTepat.services.ProdukService;
import com.finalproject.WarungTepat.services.TransaksiService;
import org.hibernate.mapping.Join;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/transaksi")
public class TransaksiController {
    @Autowired
    private TransaksiService transaksiService;

    @Autowired
    private ProdukService produkService;

    //Create Transaction
    @PostMapping
    public ResponseEntity<ResponseData<Transaksi>> createTransaksi(@Valid TransaksiRequest transaksiRequest, Errors errors) throws Exception {
        ResponseData<Transaksi> responseData = new ResponseData<>();
        if(errors.hasErrors()){
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        System.out.println("Transaksi berhasil dibuat ....!");
        responseData.setStatus(true);
        responseData.setPayload(transaksiService.save(transaksiRequest));
        return ResponseEntity.ok(responseData);
    }

    //Payment Transaction
    @PutMapping("/bayar")
    public ResponseEntity<ResponseData<Transaksi>> updateTransaksi(@Valid TransaksiRequest transaksiRequest, Errors errors) throws Exception {
        ResponseData<Transaksi> responseData = new ResponseData<>();
        if(errors.hasErrors()){
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        responseData.setStatus(true);
        responseData.setPayload(transaksiService.update(transaksiRequest));
        System.out.println();
        return ResponseEntity.ok(responseData);
    }

    //Confirm From Supplier
    @PutMapping("/konfirmasi")
    public ResponseEntity<ResponseData<Transaksi>> konfirmasiTransaksi(@Valid TransaksiRequest transaksiRequest, Errors errors) throws Exception {

        ResponseData<Transaksi> responseData = new ResponseData<>();
        if(errors.hasErrors()){
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        responseData.setStatus(true);
        responseData.setPayload(transaksiService.konfirmasi(transaksiRequest));
        return ResponseEntity.ok(responseData);
    }

    //Receive Product by Agent
    @Transactional
    @PutMapping("/diterima")
    public ResponseEntity<ResponseData<Transaksi>> terimaTransaksi(@Valid TransaksiRequest transaksiRequest, Errors errors) throws Exception {
        ResponseData<Transaksi> responseData = new ResponseData<>();
        if(errors.hasErrors()){
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        responseData.setStatus(true);
        responseData.setPayload(transaksiService.diterima(transaksiRequest));

        return ResponseEntity.ok(responseData);
    }

    //Get Customer by status and nik
    @GetMapping("/nasabah/{status}/{nik}")
    public Iterable<Transaksi> statusPesanan(@PathVariable("status") String status,@PathVariable("nik") long nikKtp){
        return transaksiService.cariStatusByNikKtp( nikKtp, status);
    }

    //Get all Transaction
    @GetMapping("/all")
    public Iterable<Transaksi> allTransaksi(){
        return transaksiService.findAll();
    }

    //Get Customer by nik
    @GetMapping("/nasabah/{nik}")
    public Iterable<Transaksi> allTransaksiByNikKtp(@PathVariable("nik") long nik){
        return transaksiService.cariTransaksiByNikKtp(nik);
    }

    //Get Join 3 Tables with params nik
    @GetMapping("/{nik}")
    public ResponseEntity<List<JoinTransaksi>> joinTransaksiResponseEntity(@PathVariable("nik") Long nik){
        return ResponseEntity.ok(transaksiService.joinTransaksi(nik));
    }

    //Get Join 3 Tables with params nohp
    @GetMapping("/supplier/{nohp}")
    public ResponseEntity<List<JoinTransaksiSupplier>> joinTransaksiSupplier(@PathVariable("nohp") String nohp){
        return ResponseEntity.ok(transaksiService.joinTransaksiSupplier(nohp));
    }

}
