package com.finalproject.WarungTepat.controllers;

import com.finalproject.WarungTepat.dto.Nasabah;
import com.finalproject.WarungTepat.dto.requests.TransaksiSaldo;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
public class NasabahController {

    //Login For Testing
    @PostMapping(value = "/login-postman")
    public ResponseEntity redirectWithJSON(@RequestBody Nasabah nasabah) {
        System.out.println("Customer login sucessfully .....!");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(URI.create("http://192.168.83.108:8084/api/nasabah/login-warung-tepat"));
        System.out.println("Di URL : "+httpHeaders);
        return new ResponseEntity(httpHeaders, HttpStatus.TEMPORARY_REDIRECT);
    }

    //Login Agent Redirect to Prospera
    @PostMapping(value = "/login")
    public ResponseEntity redirectWithForm(@RequestHeader Nasabah nasabah) {
        System.out.println("Customer login sucessfully .....!");
        HttpHeaders httpHeaders = new HttpHeaders();

        httpHeaders.setLocation(URI.create("http://192.168.83.108:8084/api/nasabah/login-warung-tepat"));
        System.out.println("Di URL : "+httpHeaders);

        return new ResponseEntity(httpHeaders, HttpStatus.TEMPORARY_REDIRECT);
    }

    //Count Saldo Supplier and Agent
    @Transactional
    @PutMapping("/transaksi-saldo")
    public ResponseEntity redirectSaldo(TransaksiSaldo transaksiSaldo) {
        System.out.println("Customer buying product......");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(URI.create("http://192.168.83.108:8084/api/tabungan/transaksi-saldo"));
        System.out.println("Di URL : "+httpHeaders);
        return new ResponseEntity(httpHeaders, HttpStatus.TEMPORARY_REDIRECT);
    }

    //Find Nasabah byNIK
    @GetMapping("/nasabah/{nik}")
    public ResponseEntity getSaldo(@PathVariable("nik") Long nik) {
        System.out.println("Customer get saldo......");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(URI.create("http://192.168.83.108:8084/api/nasabah/"+nik));
        System.out.println("Di URL : " + httpHeaders);
        return new ResponseEntity(httpHeaders, HttpStatus.TEMPORARY_REDIRECT);
    }
}
