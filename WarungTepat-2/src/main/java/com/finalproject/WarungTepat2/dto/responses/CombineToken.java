package com.finalproject.WarungTepat2.dto.responses;

import lombok.Data;

@Data
public class CombineToken {
    private String token;
    private long id;

    public CombineToken(String token, long id) {
        this.token = token;
        this.id = id;
    }
}
