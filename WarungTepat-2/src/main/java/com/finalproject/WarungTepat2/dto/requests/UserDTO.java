package com.finalproject.WarungTepat2.dto.requests;

import lombok.Data;

@Data
public class UserDTO {

	private Long idSupplier;

	private String namaSupplier;

	private String kotaSupplier;

	private String alamatSupplier;

	private Double saldo;

//	@NotEmpty(message = "Nomor Handphone dibutuhkan!")
	private String nomorHandphone;

//	@NotEmpty(message = "Password dibutuhkan!")
	private String password;

//	@NotEmpty(message = "Email dibutuhkan!")
	private String email;

}