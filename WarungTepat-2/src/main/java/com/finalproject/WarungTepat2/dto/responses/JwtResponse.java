package com.finalproject.WarungTepat2.dto.responses;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
public class JwtResponse implements Serializable {

	private static final long serialVersionUID = -8091879091924046844L;
	@NotEmpty(message = "Token belum di Generate")
	private final String jwttoken;

	private  int id;

}