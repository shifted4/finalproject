package com.finalproject.WarungTepat2.dto.responses;

import lombok.Data;

@Data
public class TransaksiDTO {
    private int idProduk;
    private String namaProduk;
    private Integer harga;
    private String kuantitas;
    private String idSupplier;
    private String namaSupplier;
    private String statusPesanan;
    private long nikKtp;
    private String namaNasabah;


}
