package com.finalproject.WarungTepat2.dto.requests;

import lombok.Data;

@Data
public class TransaksiSaldo {
    private long nikKtp;
    private long nominal;
}
