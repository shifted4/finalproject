package com.finalproject.WarungTepat2.dto.requests;

import lombok.Data;

import java.util.Date;

@Data
public class TransaksiRequest {
    private  int idTransaksi;
    private int idProduk;
    private long idSupplier;
    private long nikKtp;
    private int harga;
    private int kuantitas;
    private double total;
    private Date tanggal;
    private Date tanggalTerima;
    private boolean status;
    private String statusPesanan;
}
