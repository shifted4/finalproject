package com.finalproject.WarungTepat2.services;

import com.finalproject.WarungTepat2.models.entities.Produk;
import com.finalproject.WarungTepat2.models.repositories.ProdukRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdukService {

    @Autowired
    private ProdukRepository produkRepo;

    public Produk save(Produk produk){
        return produkRepo.save(produk);
    }

    public Produk findOne (Integer id){
        Optional<Produk> produk = produkRepo.findById(id);
        if(produk.isEmpty()){
            produk = Optional.empty();
        }
        return produk.get();
    }

    public Iterable<Produk> findAll(){
        return produkRepo.findAll();
    }

    public Iterable<Produk> findAllWithoutDeleteAt(){
        return produkRepo.findByDeleteAt();
    }

    public void removeOne(Integer id){
        produkRepo.deleteById(id);
    }

    public List<Produk> findProdukByName(String namaProduk){
        return produkRepo.findByNamaProduk(namaProduk);
    }

}
