package com.finalproject.WarungTepat2.services;

import com.finalproject.WarungTepat2.controllers.NasabahController;
import com.finalproject.WarungTepat2.models.entities.DAOUser;
import com.finalproject.WarungTepat2.models.entities.Produk;
import com.finalproject.WarungTepat2.dto.requests.TransaksiRequest;
import com.finalproject.WarungTepat2.dto.requests.TransaksiSaldo;
import com.finalproject.WarungTepat2.models.entities.Transaksi;
import com.finalproject.WarungTepat2.models.repositories.TransaksiRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class TransaksiService {
    @Autowired
    private TransaksiRepositoy transaksiRepositoy;

    @Autowired
    private ProdukService produkService;

    @Autowired
    private JwtUserDetailsService supplierService;

    @Autowired
    private NasabahController nasabahController;

    public Transaksi save(TransaksiRequest transaksiRequest) {
        Transaksi newTransaksi = new Transaksi();
        newTransaksi.setTanggalTransaksi(null);
        newTransaksi.setStatus(false);
        newTransaksi.setStatusPesanan(null);
        newTransaksi.setHarga(transaksiRequest.getHarga());
        newTransaksi.setTotal(transaksiRequest.getHarga() * transaksiRequest.getKuantitas());
        newTransaksi.setKuantitas(transaksiRequest.getKuantitas());
        newTransaksi.setIdProduk(transaksiRequest.getIdProduk());
        newTransaksi.setIdSupplier(transaksiRequest.getIdSupplier());
        newTransaksi.setNikKtp(transaksiRequest.getNikKtp());
        return transaksiRepositoy.save(newTransaksi);
    }
    public Transaksi update(TransaksiRequest transaksiRequest) {
        Transaksi newTransaksi = findOne(transaksiRequest.getIdTransaksi());
        newTransaksi.setTanggalTransaksi(new Date());
        newTransaksi.setStatus(true);
        newTransaksi.setStatusPesanan("pesan");
        return transaksiRepositoy.save(newTransaksi);
    }

    public Transaksi konfirmasi(TransaksiRequest transaksiRequest) {
        Transaksi tr = findOne(transaksiRequest.getIdTransaksi());
        tr.setStatusPesanan("dikirim");
        return transaksiRepositoy.save(tr);
    }

    public Transaksi diterima(TransaksiRequest transaksiRequest) {
        Transaksi newTransaksi = findOne(transaksiRequest.getIdTransaksi());
        newTransaksi.setTanggalTerima(new Date());;
        newTransaksi.setStatusPesanan("diterima");


        Produk produk = produkService.findOne(newTransaksi.getIdProduk());
        produk.setStok(produk.getStok() - newTransaksi.getKuantitas());

        DAOUser supplier = supplierService.findOne(newTransaksi.getIdSupplier());
        supplier.setSaldo(supplier.getSaldo() + newTransaksi.getTotal());

        TransaksiSaldo transaksiSaldo = new TransaksiSaldo();
        transaksiSaldo.setNikKtp(newTransaksi.getNikKtp());
        transaksiSaldo.setNominal(newTransaksi.getTotal());

        NasabahController nasabahController1 = new NasabahController();
        nasabahController1.redirectSaldo(transaksiSaldo);

        return transaksiRepositoy.save(newTransaksi);
    }

    public Iterable<Transaksi> cariStatusDanIdSupplier(String statusPesanan, Long idSupplier){
        return transaksiRepositoy.findAllByStatusPesananAndIdSupplier(statusPesanan,idSupplier);
    }

    public Transaksi findOne(Integer id) {
        Optional<Transaksi> pesanan = transaksiRepositoy.findById(id);
        if (pesanan.isEmpty()) {
            pesanan = Optional.empty();
        }
        return pesanan.get();
    }

    public Iterable<Transaksi> findAll() {
        return transaksiRepositoy.findAll();
    }

    public void removeOne(Integer id) {
        transaksiRepositoy.deleteById(id);
    }
}
