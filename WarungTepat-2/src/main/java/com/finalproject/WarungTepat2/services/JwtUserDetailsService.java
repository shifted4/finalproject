package com.finalproject.WarungTepat2.services;

import com.finalproject.WarungTepat2.dto.requests.UserDTO;
import com.finalproject.WarungTepat2.models.entities.DAOUser;
import com.finalproject.WarungTepat2.models.repositories.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	public static final Logger logger = LoggerFactory.getLogger(JwtUserDetailsService.class);

	@Autowired
	private UserDao userDao;

	@Autowired
	private PasswordEncoder bcryptEncoder;



	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info("Creating token login Supplier : {}", username);
		DAOUser user = userDao.findByNomorHandphone(username);

		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new User(user.getNomorHandphone(), user.getPassword(),
				new ArrayList<>());
	}

	public DAOUser save(DAOUser supplier){
		return userDao.save(supplier);
	}

	public DAOUser saveSupplier(UserDTO supplier) {
		DAOUser newUser = new DAOUser();
		newUser.setNomorHandphone(supplier.getNomorHandphone());
		newUser.setPassword(bcryptEncoder.encode(supplier.getPassword()));
		newUser.setEmail(supplier.getEmail());
		return userDao.save(newUser);
	}
	public DAOUser updateSupplier(DAOUser supplier){
		DAOUser newUser = findOne(supplier.getIdSupplier());
		System.out.println(supplier.getPassword());
		if (supplier.getPassword() != null){
			newUser.setPassword(bcryptEncoder.encode(supplier.getPassword()));
		}
			return userDao.save(newUser);
	}

	public DAOUser findOne(Long id){
		Optional<DAOUser> daoUser = userDao.findById(id);
		return daoUser.orElse(null);
	}

	public Iterable<DAOUser> findAll(){
		return userDao.findAll();
	}

	public void removeOne(Long id){
		userDao.deleteById(id);
	}

	public DAOUser findByNomorHanphone(String nomorHandphone){
		return userDao.findByNomorHandphone(nomorHandphone);
	}
}