package com.finalproject.WarungTepat2.models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "transaksi")
@AllArgsConstructor
@NoArgsConstructor
public class Transaksi {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTransaksi;

    @Column
    private int idProduk;

    @Column
    private long idSupplier;

    @Column
    private long nikKtp;

    @Column
    private int harga;

    @Column
    private int kuantitas;

    @Column
    private int total;

    @Column
    private boolean status;

    @Column
    @Temporal(TemporalType.DATE)
    private Date tanggalTransaksi;

    @Column
    @Temporal(TemporalType.DATE)
    private Date tanggalTerima;

    @Column
    private String statusPesanan;
}
