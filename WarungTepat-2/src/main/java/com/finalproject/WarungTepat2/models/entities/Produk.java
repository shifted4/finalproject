package com.finalproject.WarungTepat2.models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "produk")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Produk implements Serializable {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id_produk")
        private int idProduk;

        @NotEmpty(message = "Nama produk dibutuhkan!")
        @Column
        private String namaProduk;

        @NotNull(message = "Harga produk dibutuhkan!")
        @Column
        private Integer harga;

        @Column
        private String deskripsi;

        @NotEmpty(message = "gambar produk dibutuhkan!")
        @Column
        private String gambar;

        @NotNull(message = "Stok produk dibutuhkan!")
        @Column
        private Integer stok;

        @Column()
        private boolean status; //Nilai tambah default false

        @Column(nullable = false, updatable = false)
        @Temporal(TemporalType.DATE)
        @LastModifiedDate
        private Date createdAt;

        @Column(nullable = false)
        @Temporal(TemporalType.DATE)
        @LastModifiedDate
        private Date updatedAt;

        @Column()
        @Temporal(TemporalType.DATE)
        private Date deleteAt;

        @Column
        private long idSupplier;

        @Column
        private String kategori;

    @PrePersist
    protected void prePersist() {
        if (this.createdAt == null) createdAt = new Date();
        if (this.updatedAt == null) updatedAt = new Date();
    }
}
