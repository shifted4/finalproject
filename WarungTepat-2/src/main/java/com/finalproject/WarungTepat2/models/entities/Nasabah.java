package com.finalproject.WarungTepat2.models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Nasabah {
    public String nama;
    public  long nikKtp;
    public String email;
    public String password;
    public String noHP ;
    public String pekerjaan ;
    public String alamat ;
    public int flagWarungTepat ;
    public Date tanggalBuat;
}
