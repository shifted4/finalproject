package com.finalproject.WarungTepat2.models.repositories;

import com.finalproject.WarungTepat2.models.entities.Transaksi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransaksiRepositoy extends JpaRepository<Transaksi, Integer> {

    Iterable<Transaksi> findAllByStatusPesananAndIdSupplier(String statusPesanan, Long idSupplier);

}
