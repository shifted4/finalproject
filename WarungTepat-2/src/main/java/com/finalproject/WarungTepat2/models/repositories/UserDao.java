package com.finalproject.WarungTepat2.models.repositories;

import com.finalproject.WarungTepat2.models.entities.DAOUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao extends JpaRepository<DAOUser, Long> {
    DAOUser findByNomorHandphone(String nomorHandphone);
    List<DAOUser> findByNomorHandphoneContains(String nomorHandphone);
    List<DAOUser> findByEmail(String email);
}