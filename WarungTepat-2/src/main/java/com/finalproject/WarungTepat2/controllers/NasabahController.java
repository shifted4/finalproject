package com.finalproject.WarungTepat2.controllers;

import com.finalproject.WarungTepat2.dto.requests.TransaksiSaldo;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
public class NasabahController {
    @Transactional
    @PutMapping("/transaksi-saldo")
    public ResponseEntity redirectSaldo(@RequestBody TransaksiSaldo transaksiSaldo) {
        System.out.println("Trying Transaction amount .....!");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(URI.create("http://192.168.100.7:8080/api/tabungan/transaksi-saldo"));
        System.out.println(httpHeaders);
        return new ResponseEntity(httpHeaders, HttpStatus.TEMPORARY_REDIRECT);
    }
}
