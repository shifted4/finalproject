package com.finalproject.WarungTepat2.controllers;

import com.finalproject.WarungTepat2.configurations.JwtTokenUtil;
import com.finalproject.WarungTepat2.dto.requests.JwtRequest;
import com.finalproject.WarungTepat2.dto.requests.UserDTO;
import com.finalproject.WarungTepat2.dto.responses.CombineToken;
import com.finalproject.WarungTepat2.dto.responses.JwtResponse;
import com.finalproject.WarungTepat2.dto.responses.ResponseData;
import com.finalproject.WarungTepat2.models.entities.DAOUser;
import com.finalproject.WarungTepat2.services.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin("*")
public class JwtAuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;


	@PostMapping("/login")
	public ResponseEntity<ResponseData<?>> createAuthenticationToken(@RequestBody @Valid JwtRequest authenticationRequest, DAOUser supplier, Errors errors) throws Exception {
		ResponseData<CombineToken> responseData = new ResponseData<>();

		if(errors.hasErrors()){
			for (ObjectError error : errors.getAllErrors()) {
				responseData.getMessages().add(error.getDefaultMessage());
			}
			responseData.setStatus(false);
			responseData.setPayload(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
		}
		responseData.setStatus(true);
		authenticate(authenticationRequest.getNomorHandphone(), authenticationRequest.getPassword());
		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getNomorHandphone());
		final String token = jwtTokenUtil.generateToken(userDetails);

		DAOUser supplier1 = userDetailsService.findByNomorHanphone(authenticationRequest.getNomorHandphone());
		if (supplier1 == null){
			throw new RuntimeException("Supplier tidak ditemukan !");
		}
		long id = supplier1.getIdSupplier();

		responseData.setPayload(new CombineToken(token, id));

		return ResponseEntity.ok(responseData);
	}

	@PostMapping("/register")
	public ResponseEntity<ResponseData<?>> saveSupplier(@RequestBody @Valid  UserDTO requestSupplier, Errors errors) throws Exception {
		ResponseData<DAOUser> responseData = new ResponseData<>();
		if(errors.hasErrors()){
			for (ObjectError error : errors.getAllErrors()) {
				responseData.getMessages().add(error.getDefaultMessage());
			}

			responseData.setStatus(false);
			responseData.setPayload(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
		}
		responseData.setStatus(true);
		responseData.setPayload(userDetailsService.saveSupplier(requestSupplier));

		return ResponseEntity.ok(responseData);
	}

	@GetMapping("supplier/{id}")
	public DAOUser findOne(@PathVariable("id") Long idSupplier){
		return userDetailsService.findOne(idSupplier);
	}

	@PutMapping("/supplier")
	public ResponseEntity<ResponseData<DAOUser>> updateSupplier(@RequestBody @Valid DAOUser supplier, Errors errors){
		ResponseData<DAOUser> responseData = new ResponseData<>();
		if(errors.hasErrors()){
			for(ObjectError error : errors.getAllErrors()){
				responseData.getMessages().add(error.getDefaultMessage());
			}
			responseData.setStatus(false);
			responseData.setPayload(null);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
		}
		responseData.setStatus(true);
		responseData.setPayload(userDetailsService.updateSupplier(supplier));
		return ResponseEntity.ok(responseData);
	}

	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}

}