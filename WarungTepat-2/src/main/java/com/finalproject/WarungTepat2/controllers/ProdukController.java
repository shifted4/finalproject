package com.finalproject.WarungTepat2.controllers;

import com.finalproject.WarungTepat2.dto.responses.ResponseData;
import com.finalproject.WarungTepat2.models.entities.Produk;
import com.finalproject.WarungTepat2.services.JwtUserDetailsService;
import com.finalproject.WarungTepat2.services.ProdukService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.Date;

@RestController
@RequestMapping("/produk")
@CrossOrigin("*")
public class ProdukController {
    @Autowired
    private ProdukService produkService;

    @Autowired
    private JwtUserDetailsService supplierService;


    /* ===============================================================================*/

    @PostMapping("/tambah")
    public ResponseEntity<ResponseData<Produk>> createProduk(
            @RequestBody  Produk produk,
            @Valid Errors errors){

        System.out.println(produk);

        ResponseData<Produk> responseData = new ResponseData<>();
        /* Validation input errors*/
        if(errors.hasErrors()){
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        /*Result if not errors*/
        responseData.setStatus(true);
        responseData.setPayload(produkService.save(produk));

        return ResponseEntity.status(HttpStatus.CREATED).body(responseData);
    }

    /* ===============================================================================*/

    @GetMapping("/all-data")
    public Iterable<Produk> semuaProduk(){
        return produkService.findAll();
    }

    /* ===============================================================================*/

    @GetMapping
    public Iterable<Produk> semuaProdukTanpaDeleteAt(){
        return produkService.findAllWithoutDeleteAt();
    }

    /* ===============================================================================*/

    @GetMapping("/{id}")
    public Produk produk(@PathVariable("id") Integer id){
        if(id == null){
            throw new NullPointerException("Id tidak ditemukan ! ");
        }
        Produk produk = produkService.findOne(id);
        if(produk.getDeleteAt() != null){
            throw  new RuntimeException("Produk Sudah dihapus !");
        }
        return produk;
    }

    /* ===============================================================================*/

    @PutMapping
    public ResponseEntity<ResponseData<Produk>> update(
            @RequestBody Produk produk,
            @Valid Errors errors){

        ResponseData<Produk> responseData = new ResponseData<>();
        /* Validation input errors*/
        if(errors.hasErrors()){
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        /*Result if not errors*/
        produk.setUpdatedAt(new Date());
        responseData.setStatus(true);
        responseData.setPayload(produkService.save(produk));

        return ResponseEntity.status(HttpStatus.CREATED).body(responseData);
    }

    /* ===============================================================================*/

    @PutMapping("/delete")
    public ResponseEntity<ResponseData<Produk>> deleteProduk (
            @RequestBody Produk produk,
            @Valid Errors errors){

        ResponseData<Produk> responseData = new ResponseData<>();

        if(errors.hasErrors()){
            for (ObjectError error:errors.getAllErrors()){
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }

        if(produk.getIdProduk() == 0 ){
            throw new RuntimeException("Id "+produk.getIdProduk()+" tidak ditemukan !");
        }

        produk.setDeleteAt(new Date());
        produk.setUpdatedAt(new Date());
        System.out.println("Product deleted Succesfuly at "+produk.getDeleteAt()+ " where id "+produk.getIdProduk());
        responseData.setStatus(true);
        responseData.setPayload(produkService.save(produk));
        return ResponseEntity.ok(responseData);
    }

}
