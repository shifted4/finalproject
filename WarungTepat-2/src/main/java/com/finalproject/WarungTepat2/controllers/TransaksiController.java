package com.finalproject.WarungTepat2.controllers;

import com.finalproject.WarungTepat2.dto.responses.ResponseData;
import com.finalproject.WarungTepat2.dto.requests.TransaksiRequest;
import com.finalproject.WarungTepat2.models.entities.Transaksi;
import com.finalproject.WarungTepat2.services.TransaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/transaksi")
public class TransaksiController {
    @Autowired
    private TransaksiService transaksiService;

    @PostMapping
    public ResponseEntity<ResponseData<Transaksi>> createTransaksi(@RequestBody @Valid TransaksiRequest transaksiRequest, Errors errors) throws Exception {
        ResponseData<Transaksi> responseData = new ResponseData<>();
        if(errors.hasErrors()){
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        responseData.setStatus(true);
        responseData.setPayload(transaksiService.save(transaksiRequest));

        return ResponseEntity.ok(responseData);
    }

    @PutMapping("/bayar")
    public ResponseEntity<ResponseData<Transaksi>> updateTransaksi(@RequestBody @Valid TransaksiRequest transaksiRequest, Errors errors) throws Exception {
        ResponseData<Transaksi> responseData = new ResponseData<>();
        if(errors.hasErrors()){
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        responseData.setStatus(true);
        responseData.setPayload(transaksiService.update(transaksiRequest));

        return ResponseEntity.ok(responseData);
    }

    @PutMapping("/konfirmasi")
    public ResponseEntity<ResponseData<Transaksi>> konfirmasiTransaksi(@RequestBody @Valid TransaksiRequest transaksiRequest, Errors errors) throws Exception {

        ResponseData<Transaksi> responseData = new ResponseData<>();
        if(errors.hasErrors()){
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        responseData.setStatus(true);
        responseData.setPayload(transaksiService.konfirmasi(transaksiRequest));

        return ResponseEntity.ok(responseData);
    }

    @Transactional
    @PutMapping("/diterima")
    public ResponseEntity<ResponseData<Transaksi>> terimaTransaksi(@RequestBody @Valid TransaksiRequest transaksiRequest, Errors errors) throws Exception {
        ResponseData<Transaksi> responseData = new ResponseData<>();
        if(errors.hasErrors()){
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessages().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        responseData.setStatus(true);
        responseData.setPayload(transaksiService.diterima(transaksiRequest));

        return ResponseEntity.ok(responseData);
    }

    @GetMapping("/{status}/{id}")
        public Iterable<Transaksi> statusPesanan(@RequestBody @PathVariable("status") String status,@PathVariable("id") long id){
        return transaksiService.cariStatusDanIdSupplier(status, id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> detailPesanan(@RequestBody @PathVariable("id") int id){

        transaksiService.findOne(id);
        return ResponseEntity.ok("OK");
    }

    @GetMapping("/all")
    public Iterable<Transaksi> allTransaksi(){
        return transaksiService.findAll();
    }


}
